import { StyleSheet } from "react-native";

const basic = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f5f5',
    },
    content: {
        flex: 1,
        paddingTop: 60,
    }
});

const header = StyleSheet.create({
    container: {
        marginTop: 80
    },
    content: {
        textAlign: 'center',
        fontSize: 72,
        color: 'rgba(175, 47, 47, 0.25)',
        fontWeight: '100',
    },
});

const input = StyleSheet.create({
    container: {
        marginLeft: 20,
        marginRight: 20,
        shadowOpacity: 0.2,
        shadowRadius: 3,
        shadowColor: '#000000',
        shadowOffset: { width: 2, height: 2 },
    },
    content: {
        height: 60,
        backgroundColor: '#ffffff',
        paddingLeft: 10,
        paddingRight: 10,
    }
});

const button = StyleSheet.create({
    container: {
        alignItems: 'flex-end',
    },
    content: {
        height: 50,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: '#ffffff',
        width: 200,
        marginRight: 20,
        marginTop: 15,
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, .1)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    basic: {
        alignSelf: 'flex-end',
        padding: 7,
        borderColor: '#ededed',
        borderWidth: 1,
        borderRadius: 4,
        marginRight: 5,
    },
    text: {
        color: '#666666'
    },
    complete: {
        color: 'green',
        fontWeight: 'bold',
    },
    delete: {
        color: 'rgba(175, 47, 47, 1)'
    },
    buttons: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
});

const submit = StyleSheet.create({
    content : {
        color: '#666666',
        fontWeight: '600',
    }
});

const todo = StyleSheet.create({
    container: {
       marginLeft: 20,
       marginRight: 20,
       backgroundColor: '#ffffff',
       borderTopWidth: 1,
       borderRightWidth: 1,
       borderLeftWidth: 1,
       borderColor: '#ededed',
       paddingLeft: 14,
       paddingTop: 7,
       paddingBottom: 7,
       shadowOpacity: 0.2,
       shadowRadius: 3,
       shadowColor: '#000000',
       shadowOffset: { width: 2, height: 2 },
       flexDirection: 'row',
       alignItems: 'center',
    },
    content: {
        fontSize: 17,
    },
});

export default {
    basic,
    header,
    input,
    button,
    submit,
    todo,
};
export {
    basic,
    header,
    input,
    button,
    submit,
    todo,
}

