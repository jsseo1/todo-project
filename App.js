import "react-native-gesture-handler";
import React from 'react';
import { Provider } from "mobx-react"
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import TodoService from "./todo/present/logic/TodoService";
import TodoListScreen from "./todo/ui/screen/TodoListScreen";

const Stack = createStackNavigator();

export default function App() {
  return (
      <Provider todoService={TodoService.instance}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="오늘의 할일">
            <Stack.Screen name="오늘의 할일" component={TodoListScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
  );
}

