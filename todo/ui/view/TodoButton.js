
import React, { Component } from "react";
import { Text, TouchableHighlight } from "react-native";
import { button } from "../../../style/style";
import { observer } from "mobx-react";

@observer
class TodoButton extends Component {
    //
    render() {
        //
        const { onPress, completed, name } = this.props;

        return (
            <TouchableHighlight
                style={button.basic}
                underlayColor="#efefef"
                onPress={onPress}
            >
                <Text
                    style={[
                        button.text,
                        completed ? button.complete : null,
                        name === 'Delete' ? button.delete : null,
                    ]}
                >
                    {name}
                </Text>
            </TouchableHighlight>
        );
    }
}
export default TodoButton;
