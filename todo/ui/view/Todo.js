
import React, { Component } from "react";
import { View, Text } from "react-native";
import { todo as todoStyles, button } from "../../../style/style";
import TodoButton from "./TodoButton";
import { observer } from "mobx-react";

@observer
class Todo extends Component {
    //
    render() {
        //
        const { todo, onToggleTodo, onRemoveTodo } = this.props;

        console.log(todo);
        return (
            <View style={todoStyles.container}>
                <Text
                    style={todoStyles.content}
                >
                    {todo.title}
                </Text>
                <View style={button.buttons}>
                    <TodoButton name="Done" completed={todo.completed} onPress={() => onToggleTodo(todo.todoIndex)} />
                    <TodoButton name="Delete" onPress={() => onRemoveTodo(todo.todoIndex)} />
                </View>
            </View>
        );
    }
}
export default Todo;
