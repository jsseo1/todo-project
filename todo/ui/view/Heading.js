
import React from "react";
import { View, Text } from "react-native";
import { header } from "../../../style/style";

const Heading = () => (
  <View style={header.container}>
      <Text style={header.content}>
          Todo List
      </Text>
  </View>
);

export default Heading;
