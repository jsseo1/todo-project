
import React from "react";
import { View, Text, TouchableHighlight } from "react-native";
import { button, submit } from "../../../style/style";

const Button = ({ onAddTodo }) => (
  <View style={button.container}>
      <TouchableHighlight
          style={button.content}
          underlayColor="#efefef"
          onPress={onAddTodo}
      >
          <Text style={submit.content}>Submit</Text>
      </TouchableHighlight>
  </View>
);

export default Button;
