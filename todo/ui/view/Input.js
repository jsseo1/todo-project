
import React from "react";
import { View, TextInput } from "react-native";
import { input } from "../../../style/style";

const Input = ({ inputValue, onChangeInputValue }) => (
  <View style={input.container}>
      <TextInput
          style={input.content}
          placeholder="What needs to be done?"
          placeholderTextColor="#CACACA"
          selectionColor="#666666"
          value={inputValue}
          onChangeText={onChangeInputValue}
      />
  </View>
);

export default Input;
