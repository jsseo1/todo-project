
import React from "react";
import { View } from "react-native";
import Todo from "./Todo";

const TodoList = ({ todos, onToggleTodo, onRemoveTodo }) => {
  return (
      <View>
        {
          todos && todos.length
          && todos.map(todo => (
              <Todo
                  key={todo.todoIndex}
                  todo={todo}
                  onToggleTodo={onToggleTodo}
                  onRemoveTodo={onRemoveTodo}
              />
          )) || null
        }
      </View>
  );
}

export default TodoList;
