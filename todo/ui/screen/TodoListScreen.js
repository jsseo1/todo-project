import React, { Component } from "react";
import { View, ScrollView } from "react-native";
import Heading from "../view/Heading";
import { basic } from '../../../style/style';
import { inject, observer } from "mobx-react";
import Input from "../view/Input";
import Button from "../view/Button";
import TodoList from "../view/TodoList";

@inject('todoService')
@observer
class TodoListScreen extends Component {
    //
    componentDidMount() {
        //
        this.props.todoService.findAllTodo();
    }

    onChangeInputValue(inputValue) {
        //
        this.props.todoService.changeInputValue(inputValue);
    }

    onAddTodo() {
        //
        this.props.todoService.addTodo()
            .then(this.props.todoService.findAllTodo);
    }

    onRemoveTodo(todoIndex) {
        //
        this.props.todoService.removeTodo(todoIndex)
            .then(this.props.todoService.findAllTodo);
    }

    onToggleTodo(todoIndex) {
        //
        this.props.todoService.toggleTodo(todoIndex)
            .then(this.props.todoService.findAllTodo);
    }

    render() {
        //
        const { todos, inputValue } = this.props.todoService;

        return (
            <View style={basic.container}>
                <ScrollView keyboardShouldPersistTaps="always" style={basic.content} >
                    <Heading />
                    <Input
                        inputValue={inputValue}
                        onChangeInputValue={this.onChangeInputValue.bind(this)}
                    />
                    <TodoList
                        todos={todos}
                        onRemoveTodo={this.onRemoveTodo.bind(this)}
                        onToggleTodo={this.onToggleTodo.bind(this)}
                    />
                    <Button onAddTodo={this.onAddTodo.bind(this)}/>
                </ScrollView>
            </View>
        );
    }
}

export default TodoListScreen;
