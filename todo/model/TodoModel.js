import {decorate, observable} from "mobx";


class TodoModel {
    //
    id=0;
    title= '';
    todoIndex= 0;
    completed= false;

    constructor(todo) {
        //
        if (todo) {
            Object.assign(this, todo);
        }
    }
}

decorate(TodoModel, {
    id: observable,
    title: observable,
    todoIndex: observable,
    completed: observable,
});

export default TodoModel;
