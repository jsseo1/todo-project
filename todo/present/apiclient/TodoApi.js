import axios from 'axios';
import { API_URL } from 'react-native-dotenv';

class TodoApi {
    //
    static instance;

    findAllTodo() {
        console.log(API_URL);
        return axios.get(API_URL + '/todos').then(response => response && response.data || []);
    }

    registerTodo(todo) {
        return axios.post(API_URL + '/todos', todo).then(response => response && response.data || null);
    }

    modifyTodo(todo) {
        return axios.put(API_URL + '/todos/'+todo.id, todo).then(response => response && response.data || null);
    }

    removeTodo(todoId) {
        return axios.delete(API_URL + '/todos/'+todoId).then(response => response && response.data || null);
    }
}

TodoApi.instance = new TodoApi();

export default TodoApi;
