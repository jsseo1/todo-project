import TodoApi from "../apiclient/TodoApi";
import {action, observable, runInAction} from "mobx";
import autobind from "autobind-decorator";
import TodoModel from "../../model/TodoModel";

@autobind
class TodoService {
    //
    static instance;

    todoApi;

    @observable
    inputValue = '';

    @observable
    todos = [];

    @observable
    type = 'All';

    todoIndex = 0;

    constructor(todoApi) {
        //
        if(todoApi) this.todoApi = todoApi;
    }

    @action
    changeInputValue(inputValue) {
        //
        this.inputValue = inputValue;
    }

    @action
    async findAllTodo() {
        //
        const todos = await this.todoApi.findAllTodo();
        runInAction(() => this.todos = todos.map(todo => new TodoModel(todo)));
        return todos;
    }

    @action
    addTodo() {
        //
        if (this.inputValue.match(/^\s*$/)) {
            return;
        }
        this.todoIndex++;

        const todo = new TodoModel({
            id: this.todoIndex,
            title: this.inputValue,
            todoIndex: this.todoIndex,
            completed: false,
        });

        this.inputValue = '';
        // this.todos = [ ...this.todos, todo ];
        return this.todoApi.registerTodo(todo);
    }

    @action
    removeTodo(todoIndex) {
        //
        // this.todos = this.todos.filter(todo => todo.todoIndex !== todoIndex);
        return this.todoApi.removeTodo(todoIndex);
    }

    @action
    toggleTodo(todoIndex) {
        //
        // const index = this.todos.findIndex(todo => todo.todoIndex === todoIndex);
        // if (index >= 0) {
        //     this.todos[index].completed = !this.todos[index].completed;
        // }
        //
        // this.todos = [ ...this.todos ];

        const index = this.todos.findIndex(todo => todo.todoIndex === todoIndex);
        if (index >= 0) {
            this.todos[index].completed = !this.todos[index].completed;
            return this.todoApi.modifyTodo(this.todos[index]);
        }
        else return Promise.resolve();
    }
}

TodoService.instance = new TodoService(TodoApi.instance);

export default TodoService;
